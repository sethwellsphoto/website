
var displayIndex; //index of filmArray.videos[] chosen randomly to display large video;
var thumbnailArray = [];
var keep = []; //keep for quick indexing;
window.onLoad = render();

//fills page with proper content and sets event handlers;
function render() {
    displayIndex = Math.floor(Math.random() * 4);
    keep = ["", "", "", ""];
    populateVideos();
    events();
}

//attaches events to thumbnails;
function events() {
    document.getElementById('thumbnail0').onclick = function() {
        insert(check(escape(this.childNodes[0].innerHTML.substr(1))));
    };
    document.getElementById('thumbnail1').onclick = function() {
        insert(check(escape(this.childNodes[0].innerHTML.substr(1))));
    };
    document.getElementById('thumbnail2').onclick = function() {
        insert(check(escape(this.childNodes[0].innerHTML.substr(1))));
    };
}

//inserts a new video into main video display.
function insert(ins) {
    displayIndex = ins;
    document.getElementById('video_area').innerHTML = '<p id="play_main"></p]';
    populateVideos();
}
//parses through array for appropriate word;
//post: returns index of word in array, undefined if not in array.
function check(look) {
    for (var i = keep.length - 1; i > -1; i--) {
        if (keep[i] == look) {
            return i;
        }
    }
}

//attaches handlers for large video;
function clickHandlers() {
    var $video_area = $('#video_area');
    $video_area.click(function() {
        $video_area.html('');
        $video_area.css('background-image', 'none');
        $video_area.css('background-color', '#000000');
        $video_area.html(filmArray.videos[displayIndex].videoLarge);
    });
}

//attaches event handlers to populate large video and thumbnails;
function populateVideos() {
    populateDisplayVideo();
    populateThumbnails();
    clickHandlers();
}

//puts splash preview on large video display <*instead of stock youtube splash>.
function populateDisplayVideo() {
    var $video_area = $('#video_area');
    $video_area.css('background-image', 'url(\'' + filmArray.videos[displayIndex].thumbnail + '\')');
    $video_area.css('background-size', 'cover');
    $('#video_area p').html('&#9658;' + filmArray.videos[displayIndex].name);
}

//puts preview images on thumbnails.
function populateThumbnails() {
    thumbnailArray = [];
    for (var i = 0; i < filmArray.videos.length; i++) {
        keep[i] = escape(filmArray.videos[i].name);
        if (i !== displayIndex) {
            thumbnailArray.push(filmArray.videos[i]);
        }
    }
    for (var j = 0; j < thumbnailArray.length; j++) {
        var $thumbnail = $('#thumbnail' + j);
        $thumbnail.css('background-image', 'url(\'' + thumbnailArray[j].thumbnail + '\')');
        $thumbnail.css('background-size', 'cover');
        $('#thumbnail' + j + ' p').html('&#9658;' + thumbnailArray[j].name);
    }
}