window.onLoad = populatePhotoGrid();

function populatePhotoGrid() {
    var rand = Math.floor(Math.random() * 6);
    for (var i = 0; i < photoArray.albums.length; i++) {
        var albumPreview = 'url(' + photoArray.albums[i].links[i] + ')';
        var $element = $('#preview' + i);
        $element.css('background-image', albumPreview);
        $element.css('background-size', 'cover');
    }
}