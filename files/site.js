window.onload = function() {
	galleryNavClickHandlers();
	updateImg();
}

var photos = {
	entries: [
		{link: 'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029125943-3157ebe4-la.jpg', position: '-30px 0px', name: 'Don Keller'},
		{link: 'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115527-a0042d74-la.jpg', position: '0px 0px', name: 'Shelby Foote'},
		{link: 'http://sethwells.com/home/_data/i/galleries/MalibuDap/DSC_1545-Edit-la.jpg', position: '-20px 0px', name: 'Malibu Dap'},
		{link: 'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0657-la.jpg', position: '0px 0px', name: 'Cheyenne Galindo'}
	]
};

var photoCount = 0;

function galleryNavClickHandlers() {
	$('#prev').on('click', function() {
		if (photoCount > 0) {
			photoCount--;
			updateImg();
		}
	});
	$('#next').on('click', function() {
		if (photoCount < photos.entries.length - 1) {
			photoCount++;
			updateImg();
		}
	});
	for (var i = 0; i < photos.entries.length; i++) {
		var element = '#dot' + i;
		$dot = $(element);
		$dot.on('click', function() {
			photoCount = $(this).attr('id').replace( /^\D+/g, '');
			updateImg();
		});
	}
}

function updateImg() {
	$('.dot').css('background-color', '#b5b5b5');
	$('#dot' + photoCount).css('background-color', '#5e5e5e');
	var imgLink = 'url(' + photos.entries[photoCount].link + ')';
	console.log(imgLink);
	$img = $('div#bg_img');
	$img.css('background-image', imgLink);
	$img.css('background-position', photos.entries[photoCount].position);
	$text = $('div#hover_title');
	$text.html(photos.entries[photoCount].name);
}











