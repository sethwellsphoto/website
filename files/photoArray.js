var photoArray = {

	albums: [
		{
			name: 'jae',
			links: [
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_0940-Edit-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_0959-Edit-2-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_1055-Edit-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_0999-Edit-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_1060-Edit-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Jae/DSC_1090-Edit-la.jpg'
			]
		},

		{
			name: 'cheyenne',
			links: [
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0530-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0607-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0657-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0683-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0546-la.jpg',
				'http://sethwells.com/home/_data/i/galleries/Cheyenne/DSC_0666-la.png'
			]
		},

		{
			name: 'don',
			links: [
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029130101-7d820442-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029130043-e12f67b9-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029130006-bdd2c5e0-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029125943-3157ebe4-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029125949-b776143b-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/11/01/20131101185824-58a3b861-la.jpg'
			]
		},

		{
			name: 'shelby',
			links: [
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115406-e490bccc-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115420-eb990bc2-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115445-2c294daf-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115515-a2787524-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115527-a0042d74-la.jpg',
				'http://sethwells.com/home/_data/i/upload/2013/10/29/20131029115701-4d843981-la.jpg'
			]
		}

	]
};
