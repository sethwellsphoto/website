var filmArray = {

	videos: [
		{
			name: 'American Roulette',
			thumbnail: 'https://img.youtube.com/vi/HkKgF-e55rQ/maxresdefault.jpg',
			videoSmall: '<iframe width="160" height="90" src="https://www.youtube.com/embed/HkKgF-e55rQ" frameborder="0" allowfullscreen></iframe>',
			videoLarge: '<iframe width="680" height="383" src="https://www.youtube.com/embed/HkKgF-e55rQ?autoplay=1" frameborder="0" allowfullscreen></iframe>',
			srclink: 'https://www.youtube.com/embed/HkKgF-e55rQ?autoplay=1'
		},

		{
			name: 'Blind',
			thumbnail: 'https://img.youtube.com/vi/JyrmqzcsKnI/maxresdefault.jpg',
			videoSmall: '<iframe width="160" height="90" src="https://www.youtube.com/embed/JyrmqzcsKnI" frameborder="0" allowfullscreen></iframe>',
			videoLarge: '<iframe width="680" height="383" src="https://www.youtube.com/embed/JyrmqzcsKnI?autoplay=1" frameborder="0" allowfullscreen></iframe>',
			srclink: 'https://www.youtube.com/embed/JyrmqzcsKnI?autoplay=1'
		},

		{
			name: 'Silent Globe',
			thumbnail: 'https://img.youtube.com/vi/TqJlkAgKWts/maxresdefault.jpg',
			videoSmall: '<iframe width="160" height="90" src="https://www.youtube.com/embed/TqJlkAgKWts" frameborder="0" allowfullscreen></iframe>',
			videoLarge: '<iframe width="680" height="383" src="https://www.youtube.com/embed/TqJlkAgKWts?autoplay=1" frameborder="0" allowfullscreen></iframe>',
			srclink: 'https://www.youtube.com/embed/TqJlkAgKWts?autoplay=1'
		},

		{
			name: 'The Bell',
			thumbnail: 'https://img.youtube.com/vi/cx3AY4Q0yuM/maxresdefault.jpg',
			videoSmall: '<iframe width="160" height="90" src="https://www.youtube.com/embed/cx3AY4Q0yuM" frameborder="0" allowfullscreen></iframe>',
			videoLarge: '<iframe width="680" height="383" src="https://www.youtube.com/embed/cx3AY4Q0yuM?autoplay=1" frameborder="0" allowfullscreen></iframe>',
			srclink: 'https://www.youtube.com/embed/cx3AY4Q0yuM?autoplay=1'
		}
	]

};