This website was built using HTML5, CSS3, Javascript, and PHP.

File structure is as follows:
/website/  - *Home Folder
/website/top.html - *Head of Page
/website/bottom.html -*Bottom of Page
/website/index.php - *Home Page
/website/about.php - *About Page
/website/film.php - *Film / Video Page
/website/photo.php - *Photo Page
/website/contact.php - *Contact Page
/website/files/ - CSS/JS/IMG files
/website/files/site.css - CSS File
/website/files/site.js - Main functionality JS File
/website/files/album-show.js - Displays Album via. JS
